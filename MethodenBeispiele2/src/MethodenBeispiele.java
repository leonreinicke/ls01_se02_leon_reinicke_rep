import java.util.Scanner;
public class MethodenBeispiele {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		
	//	System.out.println("Geben Sie bitte ihren Namen ein");
	//  String name = myScanner.next();
		
		String vname = leseString("Geben Sie bitte ihren Vornamen ein");
		String nname = leseString("Geben Sie bitte ihren Nachnamen ein");
		sayhello(vname,nname);
		
		int alter = leseint("Geben Sie bitte Ihr Alter ein:");
		System.out.println("Alter: " + alter);
		
	}
		
	public static int leseint(String text) {
		System.out.println(text);
		@SuppressWarnings("resource")
		Scanner myScanner = new Scanner(System.in);
		int erg = myScanner.nextInt();
		return erg;	
	}
	
	
	
	public static String leseString(String text) {
		System.out.println(text);
		Scanner myScanner = new Scanner(System.in);
		
		String str = myScanner.next();
		
		return str;
	}

	public static void sayhello(String vname, String nname) {
		System.out.println("hello " + vname + " " + nname);
	}
	
	public static void sayhello() {
		System.out.println("hello Max");
	}

}
