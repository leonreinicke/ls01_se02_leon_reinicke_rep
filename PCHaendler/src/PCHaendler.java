import java.util.Scanner;

public class PCHaendler {
	
	public static String liesString(String text) {
		
		System.out.println(text);
		Scanner myScanner = new Scanner(System.in);
		String artikel = myScanner.next();
		return artikel;
	}
		
		
	public static int liesint(String text) {
		System.out.println(text);
		Scanner myScanner = new Scanner(System.in);
		int anzahl = myScanner.nextInt();			
		return anzahl;
	}
		
	public static double liesdouble(String text) {
		System.out.println(text);
		Scanner myScanner = new Scanner(System.in);
		double preis = myScanner.nextDouble();			
		return preis;
	}
	

	
	
	
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen
		String artikel = liesString("was m�chten Sie bestellen?");
		
		int anzahl = liesint("Geben Sie die Anzahl ein:");

	    double preis = liesdouble("Geben Sie den Nettopreis ein:");

		double mwst = liesdouble("Mehrwertsteuer:");
		

		// Verarbeiten
		double nettogesamtpreis = anzahl * preis;
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);

		// Ausgeben

		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto  in � f�r: %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto in � f�r: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");

	}

}
